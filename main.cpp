#include <iostream>

using namespace std;

class MyBigNumber{
    public:
        string sum(string num1, string num2){
            const int num1_length = num1.length();
            const int num2_length = num2.length();
            const int maxLength = (num1_length > num2_length) ? num1_length : num2_length;

            string result = "";
            int carry = 0;
            int sum = 0;
            int inum = 0;

            for(int i = 1; i < maxLength + 1; i++){
                int toanHang1 = (num1_length - i > -1) ? num1[num1_length - i] : 48;
                int toanHang2 = (num2_length - i > -1) ? num2[num2_length - i] : 48;
                sum = toanHang1 + toanHang2 + carry - 2 * 48 ;              
                inum = sum % 10;
                carry = sum / 10;
                result = char(inum + 48) + result;
                cout << "Step " << i << ": " << (char(toanHang1)) << " + " << (char(toanHang2)) << " + preCarry = " << sum << " , Write " << inum << ", Carry " << carry << " , curResult: " << result << '\n';
            }
            if (carry > 0) {
                result = char(carry + 48) + result;
                cout << "Final step: exist carry = " << carry << " , curResult: " << result << '\n';
            }

            return result;
        }
};
